package com.Karolis.audiorecognition;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.view.MenuItem;

import com.Karolis.audiorecognition.R;

public class SettingsActivity extends PreferenceActivity  {
	
static boolean active = false;
static int keyRecordLength = 0;
static int keyMfccLength = 0;


private Preference recordButton;
private Preference playButton;
private SharedPreferences mSharedPreferences;


    @SuppressLint("NewApi")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        //String action = getIntent().getAction();
        addPreferencesFromResource(R.xml.settings); 
        

		//Listeneriai veiksmams
		recordButton = (Preference) findPreference("keyButton");
	    playButton = (Preference) findPreference("recordPlayButton");


		
		// Klausymo mygtukas
		recordButton.setOnPreferenceClickListener(new OnPreferenceClickListener() {
		                @Override
		                public boolean onPreferenceClick(Preference preference) {   
		                	float[] signals = null;
		                	SilenceRemoval settingsSilenceRemoval = new SilenceRemoval();
		                	
		                	String fileName = "keyPhrase";
							// Klausymas		
		                	settingsSilenceRemoval.formatedVoice(fileName);	
		                	
		                	PrintWriter writer = null;
		           	     	// Failo ilgio rasymas --------------------------------------------------------------
		           		    try {
		           				 writer = new PrintWriter("storage/external_SD/wav/keyLength.txt", "UTF-8");
		           				} catch (FileNotFoundException
		           						| UnsupportedEncodingException e) {
		           					// TODO Auto-generated catch block
		           					e.printStackTrace();
		           				}	
		                	
		                	
		                	// Iraso skaitymas
		                	try {
								signals = MFCC.readWavFile(fileName);
							} catch (IOException | WavFileException e) {
							    // TODO Auto-generated catch block
							    e.printStackTrace();
							}
		                	
		                	//keyRecordLength = signals.length - 1;
		                	try {
		                		keyRecordLength = MFCC.getRecordDurationMillis("storage/external_SD/wav/" + fileName + ".wav");
		    				} catch (IOException e) {
		    					// TODO Auto-generated catch block
		    					e.printStackTrace();
		    				}
		                	
		                	
		                	writer.print(keyRecordLength);
		                	writer.close();
							
		                	WorkWithResults.keyWindows = MFCC.windowing(signals, WorkWithResults.hammingCoeffs, 512, 13, true,
		                			"storage/external_SD/wav/" + fileName+ ".txt");
		                	keyMfccLength = MFCC.getMfccLength();
     	              System.out.println("keyMfccLength: "+ keyMfccLength);
     	        
		                	
		                    return true;
		                }
		            });
		

		// Grojimo mygtukas
		playButton.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {   

            	audioPlayer("storage/external_SD/wav", "keyPhrase.wav");

   	
                return true;
            }
        });
		
		
    }
    
     
    // Metodas iraso grojimui
    public void audioPlayer(String path, String fileName){
        //set up MediaPlayer    
        MediaPlayer mp = new MediaPlayer();

        try {
            mp.setDataSource(path+"/"+fileName);
            mp.prepare();
            mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
        case android.R.id.home:
              finish();   
        }
        return true;
	}
	
    public void onStart() {
        super.onStart();
        active = true;
     } 

     @Override
     public void onStop() {
        super.onStop();
        active = false;
     }
    
}
