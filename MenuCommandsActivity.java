package com.Karolis.audiorecognition;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.provider.AlarmClock;
import android.provider.CalendarContract;
import android.speech.RecognizerIntent;


@TargetApi(Build.VERSION_CODES.KITKAT)
@SuppressLint("NewApi")
public class MenuCommandsActivity extends Activity{

	public static final String CALCULATOR_PACKAGE ="com.android.calculator2";
    public static final String CALCULATOR_CLASS ="com.android.calculator2.Calculator";
    
    public static final String CONTACTS_PACKAGE = "com.android.contacts";
    public static final String CONTACTS_CLASS = "com.android.contacts.DialtactsContactsEntryActivity";
    
    public static final String CAMERA_PACKAGE = "com.android.camera";
    public static final String CAMERA_CLASS = "com.android.camera.NEW_PICTURE";
    
    public static final String FB_PACKAGE = "com.facebook.katana";
    public static final String FB_CLASS = "com.facebook.katana.LoginActivity";
  
	static Intent i = new Intent();  
	String[] commandsArray = WorkWithResults.commArray;
	// komadu saraso nuskaitymas ========================================================================
	static String[] readCommands(){
	    int ind = 0;
	    String[] commands= new String[73];{	
			
		  try(BufferedReader br = new BufferedReader(new FileReader("storage/external_SD/wav/commands.txt"))) {
		        StringBuilder sb = new StringBuilder();
		        String line = br.readLine();
		        commands[ind] = line;	        
		      
		        while (line != null) {
		        	ind++;
		            sb.append(line);
		            sb.append(System.lineSeparator());
		            line = br.readLine();
		            if(line != null)
		            	commands[ind] = line;
		        }       
		            
		    } catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	  }
	   return commands;
	}
	 
	public String currentCommand(int index){	
		return commandsArray[index];
	}
	
	// Suformuotas komandų sąrašas
	public String showCommandsList()
	{
		String commandsString = "";
		
		for(int i=0;i<commandsArray.length;i++){
			
			switch (i) {
			case 9: 
				commandsString = commandsString + i + ". " + commandsArray[i] + " + vardui + su tekstu + tekstas\n";
				break;
			case 10: 
				commandsString = commandsString + i + ". " + commandsArray[i] + " + vardui + su tekstu + tekstas\n";
				break;
			case 11: 
				commandsString = commandsString + i + ". " + commandsArray[i] + " + vardui + su tekstu + tekstas\n";
				break;
			case 12: 
				commandsString = commandsString + i + ". " + commandsArray[i] + " + vardui + tekstas\n";
				break;
			case 13: 
				commandsString = commandsString + i + ". " + commandsArray[i] + " + vardui + tekstas\n";
			    break;
			case 14: 
				commandsString = commandsString + i + ". " + commandsArray[i] + " + vardui + tekstas\n";
			    break;
			case 15: 
				commandsString = commandsString + i + ". " + commandsArray[i] + " + su tekstu + tekstas\n";
			    break;
			case 16: 
				commandsString = commandsString + i + ". " + commandsArray[i] + " + su tekstu + tekstas\n";
			    break;
			case 53: 
				commandsString = commandsString + i + ". " + commandsArray[i] + " + kažkurią + valandą\n";
			    break;
			case 54: 
				commandsString = commandsString + i + ". " + commandsArray[i] + " + kažkurią + valandą\n";
			    break;
			case 57: 
				commandsString = commandsString + i + ". " + commandsArray[i] + " + svetainė.lt\n";
			    break;
			case 58: 
				commandsString = commandsString + i + ". " + commandsArray[i] + " + svetainė.lt\n";
			    break;
			case 59: 
				commandsString = commandsString + i + ". " + commandsArray[i] + " + svetainė.lt\n";
			    break;
			case 20: 
				commandsString = commandsString + i + ". " + commandsArray[i] + " + vardui\n";
			    break;
			case 72: 
				commandsString = commandsString + i + ". " + commandsArray[i] + " + vardui\n";
			    break;
			default:
				commandsString = commandsString + i + ". " + commandsArray[i] + "\n";
			}
				
		}
		
		return commandsString;
	}
	
	public String time(Boolean isDate){
		String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
		String[] words = currentDateTimeString.split(" ");
		String timeRes = null;		
		
		if(isDate){
		   timeRes = words[0] + " " + words[1] + " " + words[2] + "-oji diena.";
		}
		else{
		   timeRes= words[3];
		}

		return timeRes; 
	}

    public static String batteryLevel(Context context)
    {
        i  = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));   
        int level   = i.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
        int scale   = i.getIntExtra(BatteryManager.EXTRA_SCALE, 100);
        int percent = (level*100)/scale;
        return "Akumuliatoriaus lygis: " + String.valueOf(percent) + "%";
    }
	
	public Intent newMessage(String number, String message){      
		 i = new Intent( Intent.ACTION_VIEW, Uri.parse("smsto:"+number));
		 i.putExtra( "sms_body", message);
		    return i;
		}
	
	public Intent contacts(){
	    i.setAction(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_LAUNCHER);
	    i.setComponent(new ComponentName(
	     CONTACTS_PACKAGE,
	     CONTACTS_CLASS));
	    
	    return i;
	}
	
	public Intent calculator(){     
        i.setAction(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        i.setComponent(new ComponentName(
         CALCULATOR_PACKAGE,
         CALCULATOR_CLASS));
	    return i;
	}
	
	public Intent newEvent(){   
		i = new Intent(Intent.ACTION_EDIT);  
		i.setType("vnd.android.cursor.item/event"); // sukurti nauja ivyki
	    return i;
	}
	
	public Intent camera(){  
		i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
	    return i;
	}

	public Intent photosGallery(){  
		i = new Intent( Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
	    return i;
	}
	
	public Intent facebook(){   
		String uri = "facebook://facebook.com/inbox";
		Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
	    return i;
	}
	
	public Intent music(){   
		i = new Intent("android.intent.action.MUSIC_PLAYER");
	    return i;
	}
	
	public Intent explorer(Boolean isGoogle, Boolean isLinkOutspoken, String url){   	
	
		i = new Intent(Intent.ACTION_VIEW);
			
		if(isLinkOutspoken){
			i.setData(Uri.parse("http://www."+url));
		}
		
	    if(isGoogle){	  
		   url = "http://www.google.lt";
		   i.setData(Uri.parse(url));
		}
	    if(!isGoogle && !isLinkOutspoken){
	    	url = "http://";
	    	i.setData(Uri.parse(url));
	    }
				 
	 return i;
	}

	public Intent newEmail(String message){   
		i = new Intent(Intent.ACTION_VIEW);
		i.setType("plain/text");
		i.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
		i.putExtra(Intent.EXTRA_TEXT, message);
        
		return i;
	}

	public Intent maps(){   
		i = new Intent(android.content.Intent.ACTION_VIEW, 
			    Uri.parse("http://maps.google.com/maps?"));
        
		return i;
		}
	
	public Intent skype(){   
		i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse("skype:" + ""));
		i.setComponent(new ComponentName("com.skype.raider", "com.skype.raider.Main"));
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        
		return i;
		}
	
	void bluetooth_On_Off (Boolean isOnCommand){   
		
		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter(); 
		
		if(isOnCommand)
		    if (!mBluetoothAdapter.isEnabled()) {
		    mBluetoothAdapter.enable(); 
		}  
		
		if(!isOnCommand)
			if (mBluetoothAdapter.isEnabled())
		{
			mBluetoothAdapter.disable();
		}   
	}
	
	public void flashLightOn(){     
	     Camera cam = Camera.open(); 
		 Parameters p = cam.getParameters();
		 p.setFlashMode(Parameters.FLASH_MODE_TORCH);
		 cam.setParameters(p);
		 cam.startPreview();		
	}
	
	public Intent setAlarm(int hour){  
		i = new Intent(AlarmClock.ACTION_SET_ALARM);
	    i.putExtra(AlarmClock.EXTRA_HOUR, hour);
	    i.putExtra(AlarmClock.EXTRA_MINUTES, 0);
        
		return i;
		}
	
	@SuppressLint("NewApi")
	public Intent calendar(){  
		long startMillis = System.currentTimeMillis();
		Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
		builder.appendPath("time");
		ContentUris.appendId(builder, startMillis);
		i = new Intent(Intent.ACTION_VIEW).setData(builder.build());
        
		return i;
	}
	
	// Skambinti
	public Intent call(String number){
		i = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number));
		return i;
	}	
	
	// Gauti kontakto numeri
	public String getContactNumber(String name) throws FileNotFoundException{  		
        String foundNumber = "Not found";

		String[] temp= new String[2];
		for(int index = 0; index<WorkWithResults.formatedContactsList.size(); index++){
			temp = (String[]) WorkWithResults.formatedContactsList.get(index);
			
			 if(name.equals(temp[0])){
				 foundNumber = temp[1];
				break;
			 }
			}
		 return foundNumber;
	}
	
	int recognizeHour(String[] words){
		int hourInteger = 0;
		
		System.out.println(" fhfdhhd: " + words[2]);
		
			if(words[2].equals("pirmą") || words[2].equals("pirma"))
				hourInteger = 1;
			if(words[2].equals("antrą") || words[2].equals("antra"))
				hourInteger = 2;
			if(words[2].equals("trečią") || words[2].equals("trečia"))
				hourInteger = 3;
			if(words[2].equals("ketvirtą") || words[2].equals("ketvirta"))
				hourInteger = 4;
			if(words[2].equals("penktą") || words[2].equals("penkta"))
				hourInteger = 5;
			if(words[2].equals("šeštą") || words[2].equals("šešta"))
				hourInteger = 6;
			if(words[2].equals("septintą") || words[2].equals("septinta"))
				hourInteger = 7;
			if(words[2].equals("aštuntą") || words[2].equals("aštunta"))
				hourInteger = 8;
			if(words[2].equals("devintą") || words[2].equals("devinta"))
				hourInteger = 9;

		return hourInteger;
	}
	
	
}// pabaiga ------------------------------------------------------------------------------------
