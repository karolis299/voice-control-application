package com.Karolis.audiorecognition;

import android.annotation.SuppressLint;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Karolis
 */
@SuppressLint("NewApi")
public class DTW_custom {
 
	static int rows = 0;
	static double wd = 0;
    
//MFFC koeficientu nuskaitymas is failo -------------------------------------------------------
    public static ArrayList<Double>[] readMFCC(String filePath) throws IOException{
          int i = 0;
          ArrayList<Double>[] coefficients = (ArrayList<Double>[])new ArrayList[800];
          coefficients[i] = new ArrayList<Double>();
                  
        try(BufferedReader br = new BufferedReader(new FileReader(filePath))) {
         StringBuilder sb = new StringBuilder();
         coefficients[i] = new ArrayList<Double>();
         String line = br.readLine();
         line = line.replaceAll(",",".");
         String[] values1 = line.split("\\s+");
             for(int sInd = 0; sInd < values1.length; sInd++)
             {
                coefficients[i].add(Double.parseDouble(values1[sInd]));
                //System.out.print(coefficients[i].get(sInd) + " "); 
             }
       //System.out.print("\n"); 
         
        while (line != null) {
            i++;
            coefficients[i] = new ArrayList<Double>();
            sb.append(line);
            sb.append(System.lineSeparator());
            line = br.readLine();
            
            if(line != null)
            {
           
               
             line = line.replaceAll(",",".");

             String[] values = line.split("\\s");
             for(int sInd = 0; sInd < values.length; sInd++)
             {
 
                coefficients[i].add(Double.parseDouble(values[sInd]));
                //System.out.print(coefficients[i].get(sInd) + " "); 
             }
            }
          //System.out.print("\n"); 
        }
        }      
        rows = i;
        return coefficients;
}
    
    
public static int getMFCCLength(){
       return rows;
}
    
public static double getWarpingDistance(){
       return wd;
}
    
//Dtw algoritmas ==========================================================================
public static void DTW(List<Double>[] x, List<Double>[] y, int xLength, int yLength, int vectorLength) throws FileNotFoundException, UnsupportedEncodingException{
    	   
     int eil = yLength; //eiluciu kiekis
     int st =  xLength; //stulpeliu kiekis
    
     double d1 = 0, d2 = 0, d3 = 0;
     
     double[][] M = new double[eil+1][st+1];
     int[][] intM = new int[eil+1][st+1];
     
     

     for(int i = 0; i<1; i++){
         for(int j = 1; j<=st; j++){
                 // M[i][j] = 999;
                  M[i][j] = Double.POSITIVE_INFINITY;
         }
     }
     
     for(int i = 1; i<=eil; i++){
         for(int j = 0; j<1; j++){
                M[i][j] = 0;    
               // M[i][j] = 999;
         }
     }
           
        M[0][0] = 0;
              
     for(int row = 1; row<=eil; row++){
         for(int cell = 1; cell<=st; cell++)
             {
    
                d1 = M[row-1][cell-1] + D1(x[cell-1], y[row-1], vectorLength); //1 

        
             if(row-2>=0)
             {
                 d2 = M[row-2][cell-1] + D2(x[cell-1], y[row-2], y[row-1], vectorLength); //2   
                
             }
             else
             {
                 d2 = Double.POSITIVE_INFINITY; 
                 //d2 = 999; 
             }
              
             if(cell-2>=0)
             {
                 d3 = M[row-1][cell-2] + D3(x[cell-2], y[row-1], x[cell-1], vectorLength); //3        
             }
             else
             {
                 d3 = Double.POSITIVE_INFINITY;
                //d3 = 999;
             }
             
             //System.out.println("[" + i + "," + j + "]" + " d1: " + d1+" d2: " + d2 + " d3: "+ d3);
          
           M[row][cell] = Math.min(Math.min(d1, d2), d3);
           
           if(d1<d2 && d1<d3)
             intM[row][cell] = 1;
           
           if(d2<d1 && d2<d3)
             intM[row][cell] = 2;
           
           if(d3<d2 && d3<d1)
             intM[row][cell] = 3;
                   
         }

     }
     
     int minIndex = 0;
     int eilu, stulp ;
     double minDouble = Double.MAX_VALUE;
     
     for(int i = 0; i<=yLength; i++){
         if(minDouble > M[i][xLength]){
     	     minDouble = M[i][xLength];
             minIndex = i;
         }
     }
     
     if(minIndex > 0){
         stulp = xLength;
         eilu = minIndex;
       while (stulp > 0) {
           if(intM[eilu][stulp] == 1){
            stulp--;
            eilu--;
           }
           
           if(intM[eilu][stulp] == 2){
            stulp--;
            eilu-=2;
           }
                       
           if(intM[eilu][stulp] == 3){
            stulp-=2;
            eilu--;
           }
       }
       
       
        int skirtumasTarpEil = minIndex - eilu;
        int minInt = Math.min(xLength, skirtumasTarpEil);
        minDouble = minDouble/minInt;
       }
      wd = minDouble;
     System.out.println("rez: "+ minDouble);
}
 
// 
public static double D1(List<Double> p1, List<Double> p2, int rowLength){
    double temp = 0;
    
    for(int i = 1; i<rowLength; i++){         
     temp = temp + Math.pow(p1.get(i)-p2.get(i), 2);
    // System.out.println("p1: "+ p1.get(i) + " p2: "+ p2.get(i));
     //System.out.println(i+ " temp: "+ temp);
    }
    return Math.sqrt(temp);
}

//
public static double D2(List<Double> p1, List<Double> p2, List<Double> p3, int rowLength){
     double temp1 = 0;
     double temp2 = 0;
     
     for(int i = 1; i<rowLength; i++){         
         temp1 = temp1 + Math.pow(p1.get(i)-p2.get(i), 2);
         temp2 = temp2 + Math.pow(p1.get(i)-p3.get(i), 2);      
      }
 return 0.75*(Math.sqrt(temp1) + Math.sqrt(temp2));
}

//
public static double D3(List<Double> p1, List<Double> p2, List<Double> p3, int rowLength){
     double temp1 = 0;
     double temp2 = 0;
     
     for(int i = 1; i<rowLength; i++){         
         temp1 = temp1 +  Math.pow(p1.get(i)-p2.get(i), 2);
         temp2 = temp2 +  Math.pow(p3.get(i)-p2.get(i), 2);      
      }
 return 0.75*(Math.sqrt(temp1) + Math.sqrt(temp2));
}
 
   // Ar fraze panasi i etlaona
   static boolean isSimilar(double WarpingDistance){
  	 boolean isSimilar = false;
  	 
  	 if(WarpingDistance < 6.3)
  	 {
  		 isSimilar = true;
  	 }
  		 
  	 return isSimilar;
   }
   
}
