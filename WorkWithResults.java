package com.Karolis.audiorecognition;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import com.Karolis.audiorecognition.SilenceRemoval;
import com.Karolis.audiorecognition.R;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

@SuppressLint("NewApi")
public class WorkWithResults extends FragmentActivity implements
RecognitionListener{
	
	 private SpeechRecognizer speech = null;
	 private Intent recognizerIntent;
	 private String LOG_TAG = "VoiceRecognitionActivity";
	 private static final int RESULT_SETTINGS = 1;
	
	 Boolean isQuestion = null;
	 Boolean isOnGoogleRecognition = false;
	 static Boolean isOnPerferences = false;

     static boolean isSimilarLength = false;
     
	 private static final int VOICE_RECOGNITION_REQUEST_CODE = 1001;
	 
	 private ProgressBar progressBar;
	 static int currentRecordLength = 0;
	 static ArrayList<Double>[] keyWindows = (ArrayList<Double>[])new ArrayList[800];
	 String wholeText = null;
	 static double[] hammingCoeffs;
	 public static MyTask mtask;
	 static List<String[]> formatedContactsList = null;
     static Button info;
     final Animation animation = new AlphaAnimation(1, 0); // Change alpha from fully visible to invisible
 	 static String[] commArray = new String[73];
	 
@Override
public void onCreate(Bundle savedInstanceState) {
		   if (android.os.Build.VERSION.SDK_INT > 9) {
			   StrictMode.ThreadPolicy policy = 
			           new StrictMode.ThreadPolicy.Builder().permitAll().build();
			   StrictMode.setThreadPolicy(policy);
			   }
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
	    progressBar = (ProgressBar) findViewById(R.id.progressBar1);

		info = (Button)findViewById(R.id.info);
		
	    animation.setDuration(500); // duration - half a second
	    animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
	    animation.setRepeatCount(Animation.INFINITE); // Repeat animation infinitely
	    animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the end so the button will fade back in

	    
		 speech = SpeechRecognizer.createSpeechRecognizer(this);
		 speech.setRecognitionListener(this);
		 recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		 recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE,
		 "en");
		 recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "lt");
		 recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
		 this.getPackageName());
		 recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
		 RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
		 recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);
		
		getActionBar().setDisplayHomeAsUpEnabled(false);
		hammingCoeffs = MFCC.hamming(512);
		formatedContactsList = prepareContacts();
		commArray = MenuCommandsActivity.readCommands();

		// Raktines frazes ilgio nuskaitymas	    				
		  try(BufferedReader br = new BufferedReader(new FileReader("storage/external_SD/wav/keyLength.txt"))) {			       
		         String line = br.readLine();
		         try{
		         SettingsActivity.keyRecordLength = Integer.parseInt(line);
		         }
		         catch(NumberFormatException nfe){
		        	 
		         }
		  } catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
			// Nuskaitoma raktine fraze ----------------------------------------------------------------	
			try {
			   keyWindows = DTW_custom.readMFCC("storage/external_SD/wav/keyPhrase.txt");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  
			SettingsActivity.keyMfccLength = DTW_custom.getMFCCLength();
	
			
		checkVoiceRecognition();

	
        /** Getting a reference to the ViewPager defined the layout file */
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
 
        /** Getting fragment manager */
        FragmentManager fm = getSupportFragmentManager();
 
        /** Instantiating FragmentPagerAdapter */
        MyFragmentPagerAdapter pagerAdapter = new MyFragmentPagerAdapter(fm);
 
        /** Setting the pagerAdapter to the pager object */
        pager.setAdapter(pagerAdapter);
		
	//	showUserSettings();

		setButtonHandlers();
		enableButton(R.id.btnExit, true);
		
		mtask = (MyTask) new MyTask().execute();
	}


	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings, menu);
        return super.onCreateOptionsMenu(menu);
    }
 
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
 
        case R.id.menu_settings:
        	//mtask.cancel(true);
            Intent i = new Intent(this, SettingsActivity.class);
            startActivityForResult(i, RESULT_SETTINGS);
            break;
        }
        return true;
    }
 
 
    
    // Atpazintuvo patikrinimas
    public void checkVoiceRecognition() {
    	  // Check if voice recognition is present
    	  PackageManager pm = getPackageManager();
    	  List<ResolveInfo> activities = pm.queryIntentActivities(new Intent(
    	    RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
    	  if (activities.size() == 0) {
    		  
    		  System.out.println("not preset ======================================");    

    	   Toast.makeText(this, "Voice recognizer not present",
    	     Toast.LENGTH_SHORT).show();
    	  }
    	  else{
    		  System.out.println("preset ======================================");
    	  }
    	 }

  // Nustatymai  
    private void showUserSettings() {
        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this);
 
        StringBuilder builder = new StringBuilder();
 
        builder.append("\n Vardas: "
                + sharedPrefs.getString("prefUsername", "NULL"));
 
        builder.append("\n Klausti: "
                + sharedPrefs.getBoolean("prefSendReport", false));
 
        builder.append("\n Jautrumas: "
                + sharedPrefs.getString("prefSyncFrequency", "NULL"));
 
//        TextView settingsTextView = (TextView) findViewById(R.id.info);
 
 //       settingsTextView.setText(builder.toString());
        
    	isQuestion = sharedPrefs.getBoolean("prefSendReport", false);	
    }
	
	
	private void setButtonHandlers() {
		((Button) findViewById(R.id.btnExit)).setOnClickListener(btnClick);
		((Button) findViewById(R.id.info)).setOnClickListener(btnClick);
	}

	private void enableButton(int id, boolean isEnable) {
		((Button) findViewById(id)).setEnabled(isEnable);
	}



// Komandu vykdymas =================================================================================================
void executeCommand(String command, String command1, String command2, String command3) throws FileNotFoundException, InterruptedException{
	
		MenuCommandsActivity MenuComm = new MenuCommandsActivity();
		Boolean isExecuted = null;
		Boolean isCommandExists = false;
		Boolean isExitCommand = false;
		Boolean isNameExists = true;
		
        Builder alert = new AlertDialog.Builder(WorkWithResults.this);
	    alert.setTitle("Pranešimas");
	    alert.setNeutralButton("Gerai", null);


		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        switch (which){
		        case DialogInterface.BUTTON_POSITIVE:
		        	speech.startListening(recognizerIntent);       
                    break;
		        case DialogInterface.BUTTON_NEGATIVE:
		        	mtask = (MyTask) new MyTask().execute();
		        	break;
		        }
		    }
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(WorkWithResults.this);
		builder.setPositiveButton("Taip", dialogClickListener).setNegativeButton("Ne", dialogClickListener);

		try{
		// Skambina pasakytam vardui
	    if((command.contains(MenuComm.currentCommand(20)) && command.length()>MenuComm.currentCommand(20).length()) || 
	    	command.contains(MenuComm.currentCommand(72))){  
	    	isCommandExists = true;
	    	String tel = "";
	    	String vardas = "";
	    	
	    	if(command.contains("skambink")){
	     		vardas = command.substring(command.indexOf("skambink")+9);
	     		System.out.println("vardas1: " + vardas);
	     		tel = MenuComm.getContactNumber(vardas);
	    	}
	    	else{
	    		
	     		vardas = command.substring(command.indexOf("skambinti")+10);
	     		System.out.println("vardas2: " + vardas);
	     		tel = MenuComm.getContactNumber(vardas);
	    	}
	     		if(!tel.equals("Not found")){
	     	         startActivity(MenuComm.call(tel));
	     	        }
	     		else{
	     			isNameExists = false;
	     		}
	    }
			
	    System.out.println("*******************komnanda sar: "+ MenuComm.currentCommand(0).substring(1) +
	    		"* command: " + command + "*");
	    
     	// Parodo kiek valandų
     	if(command.contains( MenuComm.currentCommand(0).substring(1)) || command.contains(MenuComm.currentCommand(1)) ||
     			command.contains(MenuComm.currentCommand(2))){
     		isCommandExists = true;
     	//	show.setTextSize(30);
    		    //show.setText(MenuComm.time(false));
     		    alert.setMessage(MenuComm.time(false));    		
     		    alert.show();
     	}
     	
     	// Parodo data
     	if(command.contains(MenuComm.currentCommand(3)) || command1.contains(MenuComm.currentCommand(3)) ||
     	   command.contains(MenuComm.currentCommand(4)) || command.contains(MenuComm.currentCommand(5))){
     		isCommandExists = true;
     		//show.setTextSize(30);
    		   // show.setText(MenuComm.time(true));
     		       alert.setMessage(MenuComm.time(true));    		
     		       alert.show();
     	}

     	// parodo baterijos lygi***************************************************************************
     	if(command.equals(MenuComm.currentCommand(6)) || command.equals(MenuComm.currentCommand(7)) ||
     	   command.equals(MenuComm.currentCommand(8))){
     	     		isCommandExists = true;

     	     		alert.setMessage(MenuCommandsActivity.batteryLevel(getBaseContext()));    		
     	     		alert.show();
     	}
     	
     	// Naujos žinutės rašymas asmeniui
     	if(command.contains(MenuComm.currentCommand(9)) || command.contains(MenuComm.currentCommand(10)) ||
     	   command.contains(MenuComm.currentCommand(11)) || command.contains(MenuComm.currentCommand(12)) ||
     	   command.contains(MenuComm.currentCommand(13)) || command.contains(MenuComm.currentCommand(14))){
     		isCommandExists = true;
     		
     		String name = null, text = null;
     		String[] words = command.split(" ");


          if(command.contains(MenuComm.currentCommand(9)) || command.contains(MenuComm.currentCommand(10))
        		  || command.contains(MenuComm.currentCommand(14))){
        	name = words[2];
            text = command.substring(command.indexOf("tekstu") + 6);
          }
     		 
     	  if(command.contains(MenuComm.currentCommand(11))){
     		name = words[3];
     		text = command.substring(command.indexOf("tekstu") + 6);
     	  }
     	
          if(command.contains(MenuComm.currentCommand(12))){
        	name = words[2];
        	text = command.substring(command.indexOf(name) + name.length());
          }
        
          if(command.contains(MenuComm.currentCommand(13))){
        	name = words[1];
        	text = command.substring(command.indexOf(name) + name.length());
          }
          
     		String tel = MenuComm.getContactNumber(name);
     		
     		if(!tel.equals("Not found")){
     	     	 startActivity(MenuComm.newMessage(tel, text));
     	        }
     		else{
     			isNameExists = false;
     		}

     	}
     	
        // naujas eMail su tekstu
     	if((command.contains(MenuComm.currentCommand(15)) ||command.contains(MenuComm.currentCommand(16)))){   
     		isCommandExists = true;
            startActivity(MenuComm.newEmail(command.substring(command.indexOf("tekstu") + 6)));
            
        }
     	
     	// naujas tuscias eMail
     	if((command.contains(MenuComm.currentCommand(35)) || command.contains(MenuComm.currentCommand(36)))){   
     		isCommandExists = true;
            startActivity(MenuComm.newEmail(""));
            
        }
     	
     	// Spartaus rinkimo iškvietimas
     	if(command.contains(MenuComm.currentCommand(17)) ||
     	   command.contains(MenuComm.currentCommand(18)) || command.contains(MenuComm.currentCommand(19)) ||
     	   command.equals(MenuComm.currentCommand(20))){    
     		isCommandExists = true;
     		startActivity(MenuComm.contacts());
     	}            	
     	
     	// Skaiciuoklės iškvietimas
     	if(command.contains(MenuComm.currentCommand(21)) || command.contains(MenuComm.currentCommand(22))){ 
     		isCommandExists = true;
            startActivity(MenuComm.calculator());
        }
     	
     	// naujo ivykio kurimas
     	if(command.contains(MenuComm.currentCommand(50)) || command.contains(MenuComm.currentCommand(51)) ||
     	   command1.contains(MenuComm.currentCommand(50)) ||  command2.contains(MenuComm.currentCommand(51)) ||
     	   command1.contains(MenuComm.currentCommand(51)) || command.contains(MenuComm.currentCommand(52))){
     		 isCommandExists = true;
             startActivity(MenuComm.newEvent());
        }
     	
     	// kameros iškvietimas
     	if(command.contains(MenuComm.currentCommand(25)) || command.contains(MenuComm.currentCommand(26))||
     	   command.contains(MenuComm.currentCommand(27))){ 
     		isCommandExists = true;
            startActivity(MenuComm.camera());
        	}
     	
     	// albumo iškvietimas
     	if(command.contains(MenuComm.currentCommand(28)) || command.contains(MenuComm.currentCommand(29))){    
     		isCommandExists = true;
            startActivity(MenuComm.photosGallery());
        	}
     	
     	// narsykles iškvietimas
     	if(command.contains(MenuComm.currentCommand(30)) || command.contains(MenuComm.currentCommand(31))){  
     		isCommandExists = true;
            startActivity(MenuComm.explorer(false, false, ""));
        	}
     	
     	// feisbuko iškvietimas
     	if(command.contains(MenuComm.currentCommand(32)) || command.contains(MenuComm.currentCommand(33)) ||
     	   command.contains(MenuComm.currentCommand(34))){     
     		isCommandExists = true;
            startActivity(MenuComm.facebook());
        	}

     	// maps
     	if(command.contains(MenuComm.currentCommand(37)) || command1.contains(MenuComm.currentCommand(37)) || 
     	   command.contains(MenuComm.currentCommand(38))){   
     		 isCommandExists = true;
             startActivity(MenuComm.maps());
        }
     	 	
    	// grotuvo iškvietimas
     	if(command.contains(MenuComm.currentCommand(41)) || command.contains(MenuComm.currentCommand(42)) ||
     	   command1.contains(MenuComm.currentCommand(41)) || command1.contains(MenuComm.currentCommand(42)) ||
     	   command.contains(MenuComm.currentCommand(43))){   
     		isCommandExists = true;
            startActivity(MenuComm.music());
        }
     	
     	// skype
     	if(command.contains(MenuComm.currentCommand(39)) || command.contains(MenuComm.currentCommand(40))){ 
     		isCommandExists = true;
            startActivity(MenuComm.skype());
        }
     	
     	//15 bliutufo ijungimas
     	if(command.contains(MenuComm.currentCommand(44))){ 
     		isCommandExists = true;
            MenuComm.bluetooth_On_Off(true);
        }
     	
     	//16 bliutufo isjungimas
     	if(command.contains(MenuComm.currentCommand(45)) || command.contains(MenuComm.currentCommand(46))){ 
     		isCommandExists = true;
            MenuComm.bluetooth_On_Off(false);
        }
     	
     	//17 lemputes ijungimas
     	if(command.contains(MenuComm.currentCommand(47)) || command.equals(MenuComm.currentCommand(48)) || 
     	   command.contains(MenuComm.currentCommand(49))){  
     		 isCommandExists = true;
             MenuComm.flashLightOn();
        }
     	
     	//18 zadintuvas
     	if(command.contains(MenuComm.currentCommand(53)) || command.contains(MenuComm.currentCommand(54))){  
     		isCommandExists = true;
     		int hour = 0;
     		Boolean isHourParsed = false;
     		List<String[]> sentences = new ArrayList<String[]>();
     		String[] words = null;
     			
     		 sentences.add(command.split(" "));
     		 sentences.add(command1.split(" "));
     		 sentences.add(command2.split(" "));
     		 sentences.add(command3.split(" "));
     		 
	
     		for(int i = 0; i<4; i++){
     		 try{
     			words = null;
     			words = sentences.get(i);
     			System.out.println("sakinys: " + words[2]);
                if(words[2].length() <= 3){
     		        hour = Integer.parseInt(words[2].substring(0,2));
         		    isHourParsed = true;
                }
                else{
                	isHourParsed = false;
                }

     		  }
     		  catch(Exception e){  
     		  	isHourParsed = false;
     		  }
     		 
     		 if(isHourParsed==true && hour > 0){
     			 System.out.println("************ " + hour);
         		 startActivity(MenuComm.setAlarm(hour));
     		    break;
     		 }
     		}   
     		
     		if(isHourParsed==false){
     			hour = MenuComm.recognizeHour(sentences.get(0));
     			startActivity(MenuComm.setAlarm(hour));
     		}	
        }
     	
     	
     	if(command.contains(MenuComm.currentCommand(60))){  
     		isCommandExists = true;
     		startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
        	}
     	
     
     	if(command.contains(MenuComm.currentCommand(61))){  
     		isCommandExists = true;
     		startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
        	}
     	
     
     	if(command.contains(MenuComm.currentCommand(62))){  
     		isCommandExists = true;
     		startActivityForResult(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS), 0);
        	}
     	
     
     	if(command.contains(MenuComm.currentCommand(63))){  
     		isCommandExists = true;
     		startActivityForResult(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS), 0);
        	}
     	
  
     	if(command.contains(MenuComm.currentCommand(64))){  
     		isCommandExists = true;
     		startActivity(new Intent(android.provider.Settings.ACTION_DISPLAY_SETTINGS));
        	}
     	
     
     	if(command.contains(MenuComm.currentCommand(65))){ 
     		isCommandExists = true;
     		startActivityForResult(new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS), 0);
        	}
     	
    
     	if(command.contains(MenuComm.currentCommand(66))){ 
     		isCommandExists = true;
     		startActivityForResult(new Intent(android.provider.Settings.ACTION_APPLICATION_SETTINGS), 0);
        	}
     	
     
     	if(command.contains(MenuComm.currentCommand(67))){  
     		isCommandExists = true;
     		startActivity(new Intent(android.provider.Settings.ACTION_DEVICE_INFO_SETTINGS));
        	}
     	
     
     	if(command.contains(MenuComm.currentCommand(68))){ 
     		isCommandExists = true;
     		startActivityForResult(new Intent(android.provider.Settings.ACTION_SOUND_SETTINGS), 0);
        	}
     	
     	//28 atidaro google
     	if(command.contains(MenuComm.currentCommand(55)) || command.equals(MenuComm.currentCommand(56))){  
     		isCommandExists = true;
     	    startActivity(MenuComm.explorer(true, false,""));
        	}
     	
     	// atidaro nuoroda
     	if(command.contains(MenuComm.currentCommand(57)) || command.contains(MenuComm.currentCommand(58)) ||
     	   command.contains(MenuComm.currentCommand(59))){  
     		isCommandExists = true;
     		
     		String[] words = command.split(" ");
     		if(words[words.length-1].contains(".lt"))
     	     startActivity(MenuComm.explorer(false, true, words[words.length-1]));
        }
     	
     	//29 atidaro kalendoriu
     	if(command.contains(MenuComm.currentCommand(23)) || command1.contains(MenuComm.currentCommand(23))
     			|| command.equals(MenuComm.currentCommand(24))){  
     		isCommandExists = true;
     	    startActivity(MenuComm.calendar());
        	}
     	
     	//30 iseina is aplikacijos
     	if(command.contains(MenuComm.currentCommand(69)) || command.contains(MenuComm.currentCommand(70)) ||
     			command.contains(MenuComm.currentCommand(71))){  
     		isCommandExists = true;
     		isExitCommand = true;
     		
        }

     	
     	    isExecuted = true; 
		}catch(Exception e){
			isExecuted = false;
		}
     
		if(isExecuted == true && isExitCommand == false && isCommandExists == true && isNameExists == true){
		    mtask = (MyTask) new MyTask().execute();

		}
		
		if(isExecuted == false && isExitCommand == false){
		    alert.setMessage("Įvyko klaida bandant vykdyti komandą");    		
		    alert.show();
		    mtask = (MyTask) new MyTask().execute();
		}
		
		if(isExecuted == true && isExitCommand == true){
			finish();
		}
		
		if(isExecuted == true && isCommandExists == false){
			builder.setTitle("Pranešimas.");
			builder.setMessage("Tokios komandos nėra: '" + command + "' Bandyti dar karta?");    		
			builder.show();
		}
		
		if(isNameExists == false){
			builder.setTitle("Pranešimas.");
			builder.setMessage("Tokio vardo nėra: '" + command.substring(9) + "' Bandyti dar karta?");    		
			builder.show();
		}
		

	System.out.println("Ar pavyko: " + isExecuted);
}// Komadu vykdymo pabaiga ========================================================================================


// Panasaus ilgio frazes aptikimas =====================================================================
int phraseDetection(int KeySignalLength, SilenceRemoval newSilenceRemoval) throws FileNotFoundException{
	
	int CurrentSignalLength = 0;
	int lowerLimit = KeySignalLength*7/10;
	int upperLimit = KeySignalLength*11/10;
    // Iraso formavimas
	newSilenceRemoval.formatedVoice("newPhrase");	
	  
	// Iraso trukmes gavimas
	try {
		CurrentSignalLength = MFCC.getRecordDurationMillis("storage/external_SD/wav/newPhrase.wav");
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	while((CurrentSignalLength < lowerLimit || CurrentSignalLength > upperLimit*4) && SettingsActivity.active == false){
	    System.out.println("Klauso. **********************************************************");
		// Iraso formavimas
		newSilenceRemoval.formatedVoice("newPhrase");	
		// Iraso trukmes gavimas
		try {
			CurrentSignalLength = MFCC.getRecordDurationMillis("storage/external_SD/wav/newPhrase.wav");
		} catch (IOException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
	}
	
	if(SettingsActivity.active == true){
		isSimilarLength = false;
	}
	else{
		isSimilarLength = true;
	}
	 newSilenceRemoval.finish();
  return CurrentSignalLength;	
}


@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
  super.onActivityResult(requestCode, resultCode, data);

  switch (requestCode) {
  case RESULT_SETTINGS:
      showUserSettings();
      break;
  } 
 }

//kontaktu nuskaitymas =========================================================================================
@SuppressLint("DefaultLocale")
public List<String[]> prepareContacts(){
	
	List<String[]> contacts = new ArrayList<String[]>();
	
					PrintWriter contactWriter = null;				
					try {
						contactWriter = new PrintWriter("storage/external_SD/wav/contacts.txt", "UTF-8");
					} catch (FileNotFoundException
							| UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
				    }
					
					
					ContentResolver cr = getContentResolver();
			        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
			        
			        if (cur.getCount() > 0) {
			            while (cur.moveToNext()) {
			                  String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
			                  String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
			                  
			                  if(name.substring(name.length()-2).equals("as")){
			                	  name = name.substring(0, name.length()-2) + "ui";  
			                  }
			                  
			                  if(name.substring(name.length()-3).equals("ius")){
			                	  
			                	  name = name.substring(0, name.length()-3) + "iui";
			                  }
			                  
			                  if(name.substring(name.length()-2).equals("us")){
			                	  
			                	  name = name.substring(0, name.length()-2) + "ui";
			                  }
			                  
			                  if(name.substring(name.length()-1).equals("a")){
			                	  
			                	  name = name.substring(0) + "i";
			                  }
			                  
			                  if(name.substring(name.length()-2).equals("is") && !name.substring(name.length()-3).equals("tis")){
			                	  
			                	  name = name.substring(0, name.length()-2) + "iui";
			                  }
			                  
			                  if(name.substring(name.length()-3).equals("tis")){
			                	  name = name.substring(0, name.length()-3) + "čiui";			                  
			                  }
			                  
			                  if(name.substring(name.length()-1).equals("e")){
			                	  name = name.substring(0) + "i";			                  
			                  }
			                  
			                  if (Integer.parseInt(cur.getString(
			                        cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
			                     Cursor pCur = cr.query(
			                               ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
			                               null,
			                               ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
			                               new String[]{id}, null);
			                     while (pCur.moveToNext()) {
			                         String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
			                        
			                       
			                         phoneNo = phoneNo.replaceAll(" ", "");
			                         name = name.toLowerCase();
			                       //  System.out.println("name: "+ name + " phone: "+ phoneNo);
			                         contactWriter.println(name + " "+ phoneNo);
			                         String[] conatctInfo = new String[2];
			                         conatctInfo[0] = name;
			                         conatctInfo[1] = phoneNo;
			                         contacts.add(conatctInfo);
			                     }
			                    pCur.close();
			                }
			            }
			        }
			  contactWriter.close();
		return contacts;
} // kontaktu nuskaitymo pabaiga ----------------------------------



	@SuppressLint("NewApi")
	private View.OnClickListener btnClick = new View.OnClickListener() {
		@SuppressLint("NewApi")
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btnExit: {
				
                mtask.cancel(true);
				finish();           	
            	
				break;
			}
			case R.id.info: {
				
                speech.startListening(recognizerIntent);
        	    progressBar.setVisibility(View.VISIBLE);

				break;
			}
		  }
		}
	};

// Pastovaus klausymo klase ==============================================================================
@SuppressLint("NewApi")
public class MyTask extends AsyncTask<Void, Void, Void> {
	SilenceRemoval avtiveSilenceRemoval;
	    private volatile boolean running = true;
	    PrintWriter dtw_writer = null;
	    double wd = 0.0;
	    float[] fineQuantitySignals = null;
	    double warpingDistance = 0.0;

	    @Override
	    protected void onPreExecute() {
		    progressBar.setVisibility(View.VISIBLE);
		    progressBar.setIndeterminate(false);
	    	enableButton(R.id.info, false); 
		    info.setBackgroundResource(R.color.gray);
		    info.setText("Laukiama raktinės frazės...");
		    animation.cancel();
		    animation.reset();
	    	avtiveSilenceRemoval = new SilenceRemoval();
   		    try {
   				 dtw_writer = new PrintWriter("storage/external_SD/wav/dtwc.txt", "UTF-8");
   				} catch (FileNotFoundException
   						| UnsupportedEncodingException e) {
   					// TODO Auto-generated catch block
   					e.printStackTrace();
   				}	
	    }

	    @Override
	    protected void onCancelled() {
	    	speech.startListening(recognizerIntent);
	    	 //System.out.println("warpingDistance: " + wd);
	    	dtw_writer.close();
    }

	    @Override
	    protected Void doInBackground(Void... params) {

	    	

	
	        while (running) {
		    
				
	        	while(SettingsActivity.active == false && SettingsActivity.keyRecordLength>-1){


	        		
	      				 
	      				try {
	      					phraseDetection(SettingsActivity.keyRecordLength, avtiveSilenceRemoval);
	      				
	      				} catch (FileNotFoundException e1) {
	      					// TODO Auto-generated catch block
	      					e1.printStackTrace();
	      				}

	      				if(isSimilarLength == true){
	      				 // signalu paruosimas ir mfcc skaiciavimas ----------------------------------------------------------
	      					

	      				
	      				try {
	      					fineQuantitySignals = MFCC.readWavFile("newPhrase");
	      				} catch (IOException | WavFileException e) {
	      										    // TODO Auto-generated catch block
	      										    e.printStackTrace();
	      				}						
	      				ArrayList<Double>[] windows = MFCC.windowing(fineQuantitySignals, hammingCoeffs, 512, 13,false, "");
	      					
	      			       
	      			        // x - etalonas, y - lyginamas su etalonu
	      					// DTW realizacija --------------------------------------------------------------
	                    System.out.println("x: "+SettingsActivity.keyMfccLength+" y: "+MFCC.getMfccLength());
	                    

	                       try {
							DTW_custom.DTW(keyWindows, windows, SettingsActivity.keyMfccLength, MFCC.getMfccLength(), 13);
						   } catch (FileNotFoundException
								| UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						   }

	                        
	                        warpingDistance = DTW_custom.getWarpingDistance();
	                        wd = warpingDistance;
	                    
	                       
	                        boolean isPhrasesSimilar =  DTW_custom.isSimilar(warpingDistance);
	                       
	                         if(isPhrasesSimilar){  
	                        	dtw_writer.println("OK: " + warpingDistance);
	                           
	                            cancel(true);
	                        	break;
	                     	                        
	                         }
	                         else{	                        	 
	                        	 System.out.println("warpingDistance: " + warpingDistance);
	                        	 System.out.println("Bad ******************************************************");
	                        	 dtw_writer.println("BAD: " + warpingDistance);
	                         }

	      				}
	        	}

	        	 if (isCancelled()) break;
	        }
	        return null;
	    }

	}

@Override
public void onBeginningOfSpeech() {
	 progressBar.setIndeterminate(false);
	 progressBar.setMax(10);
	 Log.i(LOG_TAG, "onBeginningOfSpeech");
}


@Override
public void onBufferReceived(byte[] arg0) {
		
}


@Override
public void onEndOfSpeech() {
	 Log.i(LOG_TAG, "onEndOfSpeech");
}



@Override
public void onError(int error) {

	 progressBar.setIndeterminate(false);
	 enableButton(R.id.info, true); 
	 String errorMessage = getErrorText(error);
	 info.setBackgroundResource(R.color.orange);
	 info.setText(errorMessage);
	 animation.cancel();
	 animation.reset();

	 Log.d(LOG_TAG, "FAILED " + errorMessage); 
}



@Override
public void onEvent(int eventType, Bundle params) {
	 Log.i(LOG_TAG, "onEvent");
}



@Override
public void onPartialResults(Bundle partialResults) {
	 Log.i(LOG_TAG, "onPartialResults");
}



@Override
public void onReadyForSpeech(Bundle params) {
	info.setText("Kalbėkite");
	info.setBackgroundResource(R.color.red);
	animation.reset();
	info.startAnimation(animation);
	 Log.i(LOG_TAG, "onReadyForSpeech");	
}



@Override
public void onResults(Bundle results) {
	 Log.i(LOG_TAG, "onResults");
	 ArrayList<String> matches = results
	 .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
	 String text = "";
	 int i = 0;
	 for (String result : matches){	 
	  text += result;
	  i++;
	 }

	 
	    try {
	    	executeCommand(matches.get(0), matches.get(1), matches.get(2), matches.get(3));
	    } catch (FileNotFoundException | InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    } 
	
}



@Override
public void onRmsChanged(float rmsdB) {
	 progressBar.setProgress((int) rmsdB);
	 Log.i(LOG_TAG, "onRmsChanged: " + rmsdB);
}
	
// Gauti klaudu teksta
public static String getErrorText(int errorCode) {
  String message;
   switch (errorCode) {
   case SpeechRecognizer.ERROR_AUDIO:
   message = "Audio recording error";
   break;
   case SpeechRecognizer.ERROR_CLIENT:
   message = "Client side error";
   break;
   case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
   message = "Insufficient permissions";
   break;
   case SpeechRecognizer.ERROR_NETWORK:
   message = "Tinklo klaida";
   break;
   case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
   message = "Network timeout";
   break;
   case SpeechRecognizer.ERROR_NO_MATCH:
   message = "No match";
   break;
   case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
    message = "Serveris pernelyg užimtas";
    break;
    case SpeechRecognizer.ERROR_SERVER:
    message = "Nėra tinklo ryšio";
    break;
    case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
    message = "Nieko nepasakėte";
    break;
    default:
    message = "Didn't understand, please try again.";
    break;
   }
    return message;
   }

}