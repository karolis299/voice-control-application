package com.Karolis.audiorecognition;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.TextView;


public class MyFragment extends Fragment{
	
	int mCurrentPage;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		/** Getting the arguments to the Bundle object */
		Bundle data = getArguments();
		
		/** Getting integer data of the key current_page from the bundle */
		mCurrentPage = data.getInt("current_page", 0);
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.myfragment_layout, container,false);				
		TextView tv = (TextView ) v.findViewById(R.id.tv);
		tv.setTextSize(20);
		

		
		if(mCurrentPage == 1){
			tv.setText(new MenuCommandsActivity().showCommandsList());
		}
		if(mCurrentPage == 2){
			tv.setText("* Sakykite raktinę frazę, kad aktyvuoti atpažintuvą.\n\n* Sakykite komandą iš sąrašo, kad ją vykdyti.\n\n"
					+ "* Slinkite į šonus, kad naviguoti.");
		}
		if(mCurrentPage == 3){
			tv.setText("abc");
		}
		return v;		
	}
}
