package com.Karolis.audiorecognition;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.Karolis.audiorecognition.WorkWithResults.MyTask;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

public class SilenceRemoval extends Activity {


private static final String TAG = WorkWithResults.class.getSimpleName();
private static final int RECORDER_BPP = 16;
private static final String AUDIO_RECORDER_FILE_EXT_WAV = ".wav";
private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
private static final String AUDIO_RECORDER_TEMP_FILE = "record_temp.raw";

static FileOutputStream outStream = null;

static int bufferSize;
static int frequency = 16000;
static int channelConfiguration = AudioFormat.CHANNEL_IN_MONO;
static int audioEncoding = AudioFormat.ENCODING_PCM_16BIT;
boolean started = false;

static short threshold=2000; // Nuo kokio garsumo pradedama irasineti
boolean debug=false;

public void formatedVoice(String fileName) {
	   int silenceCount = 0;
	   int soundCount = 0;
	   int tempLimit = 150;
	   
       Log.w(TAG, "doInBackground");
       try {
               String filename = getTempFilename();

           try {
                       outStream = new FileOutputStream(filename);
           } catch (FileNotFoundException e) {
                       e.printStackTrace();
           }   


           bufferSize = AudioRecord.getMinBufferSize(frequency, 
           channelConfiguration, audioEncoding); 

           AudioRecord audioRecord = new AudioRecord( MediaRecorder.AudioSource.MIC, frequency, 
                   channelConfiguration, audioEncoding, bufferSize); 

           short[] buffer = new short[bufferSize];
           // Pradeda irasineti
           if(audioRecord.getState()==android.media.AudioRecord.STATE_INITIALIZED)
           audioRecord.startRecording();    
    
          // startTime = SystemClock.uptimeMillis();
           
           //Kol tyla trunka iki 1 sek.
           while (silenceCount < tempLimit) {
        	   

        	   if(soundCount > 150) // iraso apribojimas iki 7.5 sek
       	        break; 
               int bufferReadResult = audioRecord.read(buffer, 0,bufferSize);
             
               if(AudioRecord.ERROR_INVALID_OPERATION != bufferReadResult){
                     //check signal
                   //put a threshold
                     int foundPeak=searchThreshold(buffer,threshold);
                       if (foundPeak>-1){ //found signal                 
                    	   soundCount = soundCount + 1; 
                    	   silenceCount = 0;   
           
                           byte[] byteBuffer = ShortToByte(buffer,bufferReadResult); //irasome signala
                       try {
                               outStream.write(byteBuffer);
                       } catch (IOException e) {
                               e.printStackTrace();
                       }
                       }else{
                    	// skaiciuojame kiek kartu is eiles atpazinta tyla
                        // neirasinejame signalo
                    	   silenceCount = silenceCount + 1;                      
                       }

                               //show results
                       //here, with publichProgress function, if you calculate the total saved samples, 
                       //you can optionally show the recorded file length in seconds:  publishProgress(elsapsedTime,0);                 
               }
        	   if(soundCount > 10)
        		   tempLimit = 60;

           }


           audioRecord.stop();
           //failas uzdaromas
             try {
                   outStream.close();
             } catch (IOException e) {
                   e.printStackTrace();
             }

            copyWaveFile(getTempFilename(),getFilename(fileName));
            deleteTempFile();

       } catch (Throwable t) {
           t.printStackTrace();
           Log.e("AudioRecord", "Recording Failed");
       }
    
   } //pabaiga formated voice ------------------------------------------------------------



    static byte [] ShortToByte(short [] input, int elements) {
     int short_index, byte_index;
     int iterations = elements; //input.length;
     byte [] buffer = new byte[iterations * 2];

     short_index = byte_index = 0;

     for(/*NOP*/; short_index != iterations; /*NOP*/)
     {
       buffer[byte_index]     = (byte) (input[short_index] & 0x00FF); 
       buffer[byte_index + 1] = (byte) ((input[short_index] & 0xFF00) >> 8);

       ++short_index; byte_index += 2;
     }

     return buffer;
   }

   static int searchThreshold(short[]arr,short thr){
       int peakIndex;
       int arrLen=arr.length;
       for (peakIndex=0;peakIndex<arrLen;peakIndex++){
           if ((arr[peakIndex]>=thr) || (arr[peakIndex]<=-thr)){
               //se supera la soglia, esci e ritorna peakindex-mezzo kernel.

               return peakIndex;
           }
       }
       return -1; //not found
   }

   private static String getFilename(String name){
       //String filepath = Environment.getExternalStorageDirectory().getPath();
       //File file = new File(filepath,AUDIO_RECORDER_FOLDER);
       File file = new File("storage/external_SD/wav");
       
       if(!file.exists()){
               file.mkdirs();
       }

       return (file.getAbsolutePath() + "/" + name + AUDIO_RECORDER_FILE_EXT_WAV);
   }


   public static String getTempFilename(){
       String filepath = Environment.getExternalStorageDirectory().getPath();
       File file = new File(filepath,AUDIO_RECORDER_FOLDER);
	   //File file = new File("storage/external_SD/wav_temp");

       if(!file.exists()){
               file.mkdirs();
       }
       
       File tempFile = new File(filepath,AUDIO_RECORDER_TEMP_FILE);

       if(tempFile.exists())
               tempFile.delete();

       
       return (file.getAbsolutePath() + "/" + AUDIO_RECORDER_TEMP_FILE);
   }


   private static void deleteTempFile() {
           File file = new File(getTempFilename());

           file.delete();
   }

   public static void copyWaveFile(String inFilename,String outFilename){
       FileInputStream in = null;
       FileOutputStream out = null;
       long totalAudioLen = 0;
       long totalDataLen = totalAudioLen + 36;
       long longSampleRate = frequency;
       int channels = 1;
       long byteRate = RECORDER_BPP * frequency * channels/8;

       byte[] data = new byte[bufferSize];

       try {
               in = new FileInputStream(inFilename);
               out = new FileOutputStream(outFilename);
               totalAudioLen = in.getChannel().size();
               totalDataLen = totalAudioLen + 36;


               WriteWaveFileHeader(out, totalAudioLen, totalDataLen,
                               longSampleRate, channels, byteRate);

               while(in.read(data) != -1){
                       out.write(data);
               }

               in.close();
               out.close();
       } catch (FileNotFoundException e) {
               e.printStackTrace();
       } catch (IOException e) {
               e.printStackTrace();
       }
   }

   public static void WriteWaveFileHeader(
                   FileOutputStream out, long totalAudioLen,
                   long totalDataLen, long longSampleRate, int channels,
                   long byteRate) throws IOException {

           byte[] header = new byte[44];

           header[0] = 'R';  // RIFF/WAVE header
           header[1] = 'I';
           header[2] = 'F';
           header[3] = 'F';
           header[4] = (byte) (totalDataLen & 0xff);
           header[5] = (byte) ((totalDataLen >> 8) & 0xff);
           header[6] = (byte) ((totalDataLen >> 16) & 0xff);
           header[7] = (byte) ((totalDataLen >> 24) & 0xff);
           header[8] = 'W';
           header[9] = 'A';
           header[10] = 'V';
           header[11] = 'E';
           header[12] = 'f';  // 'fmt ' chunk
           header[13] = 'm';
           header[14] = 't';
           header[15] = ' ';
           header[16] = 16;  // 4 bytes: size of 'fmt ' chunk
           header[17] = 0;
           header[18] = 0;
           header[19] = 0;
           header[20] = 1;  // format = 1
           header[21] = 0;
           header[22] = (byte) channels;
           header[23] = 0;
           header[24] = (byte) (longSampleRate & 0xff);
           header[25] = (byte) ((longSampleRate >> 8) & 0xff);
           header[26] = (byte) ((longSampleRate >> 16) & 0xff);
           header[27] = (byte) ((longSampleRate >> 24) & 0xff);
           header[28] = (byte) (byteRate & 0xff);
           header[29] = (byte) ((byteRate >> 8) & 0xff);
           header[30] = (byte) ((byteRate >> 16) & 0xff);
           header[31] = (byte) ((byteRate >> 24) & 0xff);
           header[32] = (byte) (channels * 16 / 8);  // block align
           header[33] = 0;
           header[34] = RECORDER_BPP;  // bits per sample
           header[35] = 0;
           header[36] = 'd';
           header[37] = 'a';
           header[38] = 't';
           header[39] = 'a';
           header[40] = (byte) (totalAudioLen & 0xff);
           header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
           header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
           header[43] = (byte) ((totalAudioLen >> 24) & 0xff);

           out.write(header, 0, 44);
   }
}